# Othello Game / Reversi

## General info
It's a two player game which will be played in one device. The game will be executed
in Cmd, it's a c++ file.
Players info will be saved with file database.


## How to use
Choose new game button in the menu, then Two players must fill 
the information form, and you can choose the dimensions of the playground.

At the end the winner of the game will be determined, and after playing multiple times you can
check the ranking table.

## About the Game
There are sixty-four identical game pieces called disks, which are light on one side
and dark on the other. Players take turns placing disks on the board with their assigned 
color facing up. 

During a play, any disks of the opponent's color that are in a straight
line and bounded by the disk just placed and another disk of the current player's color are
turned over to the current player's color.

The objective of the game is to have the majority of 
disks turned to display one's color when the last playable empty square is filled. 